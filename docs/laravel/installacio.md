## Instal·lació del Laravel Framework

Hi ha dues formes d'instal·lar Laravel: en el propi sistema, instal·lant
prèviament tots els requeriments (servidor web, llenguatge PHP, etc.) amb les
extensions necessàries, o utilitzant una màquina virtual preparada.

El segon cas resulta còmode per una instal·lació en un entorn de
desenvolupament. Laravel ja proporciona una màquina virtual amb tot el
programari i configuracions necessàries.

Nosaltres hem d'instal·lar la màquina virtual i compartir algunes carpetes
entre el nostre sistema i la màquina virtual, de manera que poguem anar
desenvolupant la web que fem i que la màquina virtual vegi els canvis
automàticament.

La informació detallada d'aquest tipus d'instal·lació es pot trobar a
https://laravel.com/docs/5.2/homestead.

Nosaltres desenvoluparem la primera opció.

### Instal·lació dels prerequisits

Necessitarem instal·lar un servidor web, un servidor de bases de dades, i
el llenguatge PHP amb algunes extensions.

#### Debian

Instal·larem els següents paquets:

- Servidor web: *apache2*
- Extensió d'apache per usar PHP: *libapache2-mod-php5*
- Extensió de criptorgrafia pel PHP: *php5-mcrypt* (no és imprescindible en
  general, però sí si la nostra web necessita validació d'usuaris)
- Servidor de base de dades: *mariadb-server*
- Extensió del PHP per connectar-se a MySQL/MariaDB: *php5-mysql*

#### Windows

Podem utilitzar un paquet que inclogui totes les dependències, com el
[XAMPP](https://www.apachefriends.org/index.html).

### Instal·lació del Composer

El *Composer* és un programa PHP en línia de comandes que facilita la
instal·lació i la gestió de dependències entre paquets PHP.

#### Instal·lació del Composer en Debian

Cal descarregar un sol fitxer de https://getcomposer.org/download/.

Creem un directori on desenvoluparem el nostre projecte, i copiem el fitxer
descarregat allà.

#### Instal·lació del Composer en Windows

- Baixem l'instal·lador d'aquesta url:
https://getcomposer.org/doc/00-intro.md#downloading-the-composer-executable

- En el procéss d'instal·lació arriba un moment en què ens demanarà el path del
nostre *php.exe*. Aquest fitxer es troba a la carpeta *php* del *xampp*.

El principal avantatge d'utilitzar l'instal·lador és que ens afegeix el
*composer* i el *php* al PATH del sistema.

Si en comptes d'utilitzar l'instal·lador, baixem directament el fitxer, podem
seguir com en la instal·lació per Debian, però hem d'afegir el *php* a mà
al PATH:

En Windows, el PHP pot no estar a la ruta del sistema. Per afegir-lo:

- Botó dret a l'ordinador.
- Seleccionem *Propietats*.
- Cliquem a *Configuració avançada del sistema*.
- Cliquem a *Variables d'entorn...*.
- Seleccionem *PATH* i cliquem a *Edita...*
- Al final del *PATH* afegim un punt i coma seguit de la ruta on hi ha
l'executable del PHP, per exemple `d:\xampp\php`.

### Instal·lació del Laravel

Utilitzarem el Composer per baixar el Laravel i crear l'estructura de la nostra
web.

En una terminal, ens situem al directori on hem situat el Composer i executem:

```
php composer.phar global require "laravel/installer"
php composer.phar create-project --prefer-dist laravel/laravel <nom_projecte>
```

Si utilitzem Windows i hem instal·lat el Composer amb l'instal·lador, les dues
instruccions anteriors es transformen en:

```
composer global require "laravel/installer"
composer create-project --prefer-dist laravel/laravel <nom_projecte>
```

La primera instrucció descarrega l'instal·lador de Laravel i la segona ens
crea l'estructura de la nostra web i baixa totes les dependències del
framework.

#### Resolució de problemes

1. En Windows, la instal·lació es para i es queixa que el PHP no té l'extensió
d'OpenSSL activada.

    Per resoldre això, hem d'editar el fitxer *php.ini* (ho podem fer
    directament des del panell de control del XAMPP o cercant-lo a la
    instal·lació del PHP). A dins del fitxer cerquem *ssl* i trobarem una
    línia com: `;extension=php_openssl.dll` i esborrar el punt i coma del
    principi. Guardem el fitxer i tanquem i tornem a obrir el terminal.

2. La instal·lació es para a mitges i demana un token de seguretat de github.

    Entrem a la web de github i ens validem amb el nostre usuari (si no en
    tenim caldrà crear-ne un). Un cop validats, copiem l'adreça que ens ha
    sortit a la línia de comandes i l'enganxem al navegador. Ens movem al final
    i cliquem al botó que hi ha per generar un token. Copiem el token i
    l'enganxem al terminal (no es mostrarà). Premem enter i la instal·lació
    continuarà.

### Configuració de la instal·lació

Hem d'assegurar que el servidor web tindrà accés d'escriptura a dos directoris
concrets de la instal·lació. En un entorn de desenvolupament, n'hi ha prou
amb deixar que tothom hi pugui accedir.

Els directoris són: *storage* i *bootstrap/cache*.

En Debian, ens situem a l'arrel del projecte i executem:
`chmod -R 777 storage bootstrap/cache`.

Si utilitzem Windows amb XAMPP aquest pas no fa falta, ja que el XAMPP
s'està executant amb el mateix usuari amb què desenvolupem i, per tant, té tots
els permisos sobre tots els seus fitxers.

### Configuració de l'Apache

Tenim dues opcions per fer que l'Apache localitzi i sigui capaç de mostrar la
nostra web.

La primera és situar la web directament on l'Apache espera trobar-la: a
`/var/www` en Debian o al directori `htdocs` del XAMPP en Windows.

L'altra manera és modificar l'Apache per tal que cerqui la web on nosaltres
la volem tenir.

Per fer això cal editar la configuració de l'Apache, que trobarem a
`/etc/apache2/sites-available/000-default.conf` en Debian, o a l'opció
*config* del panell de control del XAMPP en Windows (*httpd.conf*).

Hem de localitzar la directiva *DocumentRoot* i posar la ruta a la nostra web:

```
DocumentRoot <ruta a l'arrel del nostre projecte>/public
```

A banda d'això, hem de garantir que l'Apache interpretarà les directives de
configuració que trobi en el nostre projecte en forma de fitxer *.htaccess*.

Per fer-ho, editem `/etc/apache2/apache2.conf` en Debian o la configuració
de l'Apache al panell de control del XAMPP en Windows (*httpd.conf*), cerquem
la directiva `*<Directory>` que faci referència al directori arrel i ens
assegurem que aparegui l'opció `AllowOverride All` en comptes de
`AllowOverride None`.

Si hem modificat el `DocumentRoot` abans, també hem de modificar la ruta aquí.

Al final, haurem de tenir alguna cosa com:

```
<Directory <ruta a l'arrel del nostre projecte>/public/>
        ...
        AllowOverride All
        ...
</Directory>
```

Reiniciem el servidor: `systemctl restart apache2` en Debian, o a través del
panel de controll del XAMPP en Windows.

### Comprovació

Utilitzem el navegador web i ens connectem a *localhost*. Hauríem de veure una
pàgina en blanc i un títol al mig amb el missatge *Laravel 5*.

### Configuració de l'aplicació

Finalment, hi ha alguns aspectes dels fitxers *.env* i *config/app.php* que ens
interessa revisar.

El fitxer *.env* conté paràmetres de configuració que són propis de cada
lloc on l'aplicació es pot desplegar, i de cada desenvolupador. Per tant,
aquest fitxer no s'ha d'incloure a un programa de control de versions.

En aquest fitxer podem configurar les dades d'accés a la base de dades de la
nostra aplicació.

A *config/app.php* hi ha paràmetres de configuració generals de l'aplicació.
Alguns d'ells (els que utilitzen la funció *env()*, s'estan agafant directament
del fitxer anterior).

A *app.php* podem configurar coses com el fus horari i l'idioma de l'aplicació.

Més informació sobre la configuració a
https://laravel.com/docs/5.2/configuration.
