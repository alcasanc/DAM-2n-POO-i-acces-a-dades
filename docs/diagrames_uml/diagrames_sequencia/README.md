Diagrames de seqüència
------------------------

Els **diagrames d'interacció** descriuen com un grup d'objectes col·laboren
en algun comportament. Dels diversos diagrames d'interacció que hi ha el
**diagrama de seqüència** és el més habitual.

Aquest diagrama mostra un conjunt d'objectes que exemplifiquen un
escenari concret i mostra els missatges que es passen aquests objectes
dins d'un cas d'ús concret. Els diagrames de seqüència són útils per
entendre la interacció entre diversos objectes d'un programa, però no
tant per representar complexitats algorísmiques concretes, com bucles i
condicionals.

Els objectes interactuen entre si amb l'enviament de missatges. La
recepció d'un missatge sol significar l'execució d'un mètode que
s'especifica en el missatge, i es torna actiu l'objecte mentre dura
l'execució del mètode.

![Exemple de diagrama de seqüència](docs/diagrames_uml/imatges/exemple_diagrama_sequencia.png)

Els elements són els següents:

* Una **línia de vida** representa un element connectable que participa
en una interacció enviant i rebent missatges. Una línia de vida representa
l'interval de temps en què existeix la instància o instàncies que formen
part de l’element connectable, des de la seva creació fins a la seva
destrucció, tot i que en general només durant una part o diverses
parts d’aquest interval participen en la interacció que conté la línia
de vida; aquestes parts s’anomenen activacions i representen els
intervals de temps durant els quals s’està executant alguna operació
que té com a objecte de context alguna de les seves instàncies.

* Un **missatge** és l'especificació de la comunicació entre objectes. Un
missatge es representa amb una fletxa de l'emissor cap al receptor;
diferents tipus de missatge es representen amb diferents tipus de
fletxa:

 * Un *missatge asíncron* s'indica amb una fletxa de línia contínua i
 punta oberta; un missatge és asíncron si l’objecte que l'emet no espera
 la resposta a aquest missatge abans de continuar. Aquests missatges es
 donen per exemple en l'activació de threads.

 * Un *missatge síncron* es representa amb una fletxa de línia contínua
 i punta plena; un missatge és síncron quan l'objecte que l'emet espera
 la resposta a aquest missatge abans de continuar amb el seu treball.
 Totes les crides a mètodes són missatges síncrons.

 * Un *missatge de resposta d’un missatge síncron* s'indica amb una
 fletxa de línia discontínua i punta plena.

 * Un *missatge que provoca la creació d’un objecte* s'indica amb una
 fletxa de línia discontínua i punta oberta; a més, l’extrem del
 missatge coincideix amb el començament de la línia de vida del receptor.

![Tipus de missatges](docs/diagrames_uml/imatges/tipus_fletxes_sequencia.png)

Suposem que tenim una comanda i que invoquem un dels seus mètodes per
obtenir el seu preu. Per fer això, la comanda ha de mirar totes les
seves línies i determinar el preu de cadascuna, que es basa en les
regles per al preu del producte corresponent a la línia. Un cop s'ha fet
això per cada línia, la comanda ha de calcular un descompte general, que
es basa en les regles associades al client.

#### Exemple

El següent diagrama mostra una implementació d'aquest escenari:

![Exemple comandes 1](docs/diagrames_uml/imatges/exemple_comandes.png)

En el diagrama no es mostra que les crides a *getQuantity*,
*getProduct*, *getPricingDetails* i *calculateBasePrice* s'han de fer
per a cadascuna de les línies de la comanda, mentre que la crida a
*calculateDiscounts* només s'ha de fer una vegada. Encara que existeix
notació per indicar això en un diagrama de seqüència, no la farem, i ja
hem comentat que l'objectiu és entendre la interacció entre els
objectes, no els detalls de l'algorisme.

La següent figura representa una altra forma de resoldre el mateix
problema, però els objectes interaccionen d'una manera molt diferent. La
comanda demana a cada línia que calculi el seu propi preu. La línia
delega aquest càlcul al producte. De forma similar, per calcular el
descompte, la comanda invoca un mètode en el client. Com que el client
necessita informació de la comanda per poder-ho fer, el client fa una
crida reentrant (*getBaseValue*) a la comanda per obtenir les dades.

La primera cosa a notar sobre aquests dos diagrames és la claredat amb
què mostren la diferent forma d'interaccionar dels participants. Aquesta
és la gran fortalesa dels diagrames de seqüència.

El segon punt a notar és la clara diferència d'estils entre les dues
solucions. En el primer cas s'utilitza una forma de **control
centralitzat**, en què un participant fa tot el procediment i els altres
participants es limiten a proporcionar les dades necessàries. La segona
solució utilitza un **control distribuït**, en què el procés es divideix
entre diversos participants, i cadascun d'ells fa una petita part de
l'algorisme.

![Exemple comandes 2](docs/diagrames_uml/imatges/exemple_comandes_001.png)

Els dos estils tenen els seus avantatges i inconvenients. El control
centralitzat és més senzill, perquè tot el procés es realitza en un sol
lloc, i és més fàcil d'entendre per aquells que són nous en la
programació orientada a objectes. Amb el control distribuït, en canvi,
es té la sensació de perseguir els objectes, intentant trobar on és el
programa.

Tot i això, un dels objectius principals d'un bon disseny és localitzar
els efectes del canvi. Les dades i el comportament que utilitza aquestes
dades normalment canvien a la vegada. Així que posar les dades i el
comportament que les utilitza junts en un sol lloc és la primera norma
del disseny orientat a objectes.

A més, amb el control distribuït, creem més oportunitats per utilitzar
el polimorfisme en comptes d'utilitzar la lògica condicional. Si els
algorismes per determinar el preu d'un producte són diferents per
diferents tipus de producte, el mecanisme del control distribuït ens
permet utilitzar una subclasse de producte per gestionar aquestes
variacions.

- - -

* [Exercicis](exercicis.md)
