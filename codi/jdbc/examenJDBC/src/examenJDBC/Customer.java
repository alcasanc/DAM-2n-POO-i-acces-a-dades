package examenJDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Customer {
	public final int id;
	public final String firstName;
	public final String lastName;
	
	public Customer(int id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public static Customer byId(int id) throws SQLException {
		Customer customer = null;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select customer_id, first_name, last_name from customer where customer_id = ?");
		) {
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				customer = new Customer(rs.getInt(1), rs.getString(2), rs.getString(3));
			}
			rs.close();
			return customer;
		}
	}
	
	public static List<Customer> byLastName(String lastName) throws SQLException {
		List<Customer> customers = new ArrayList<Customer>();
		try (
			PreparedStatement st = DB.connection().prepareStatement("select customer_id, first_name, last_name from customer where last_name = ?");
		) {
			st.setString(1, lastName);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				customers.add(new Customer(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}
			rs.close();
			return customers;
		}
	}
	
	public double getBalance() throws SQLException {
		double balance=0;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select get_customer_balance(?, now())");
		) {
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			rs.next();
			balance = rs.getDouble(1);
			rs.close();
		}
		return balance;
	}
	
	public void cancelDebt(Staff staff, double amount) throws SQLException {
		try (
			PreparedStatement stPayment = DB.connection().prepareStatement("insert into payment(customer_id, staff_id, amount, payment_date) values(?, ?, ?, now())");
		) {				
			stPayment.setInt(1, id);
			stPayment.setInt(2, staff.id);
			stPayment.setDouble(3, amount);
			stPayment.executeUpdate();
		}
	}
	
	public String toString() {
		return id+" - "+firstName+" "+lastName;
	}
}
