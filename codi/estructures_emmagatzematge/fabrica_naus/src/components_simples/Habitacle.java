package components_simples;

import components_abstractes.ComponentSimple;

public class Habitacle extends ComponentSimple {
	private int maxHabitants;

	public Habitacle(int maxHabitants, int pes) {
		super(pes);
		setMaxHabitants(maxHabitants);
		setNom("Habitacle");
	}

	public int getMaxHabitants() {
		return maxHabitants;
	}

	public void setMaxHabitants(int maxHabitants) {
		if (maxHabitants<0)
			throw new IllegalArgumentException("Els habitants màxims no poden ser negatius");
		this.maxHabitants = maxHabitants;
	}

	@Override
	public String toString() {
		return getNom()+"; habitants màxims: "+getMaxHabitants()+", pes: "+getPes();
	}
}
