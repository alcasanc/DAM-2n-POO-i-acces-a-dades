package enter_llarg;

import java.util.Scanner;

public class MainEnterLlarg {
	public static void tests() {
		EnterLlarg llarg = new EnterLlarg("-123456789123456789123456789");
		System.out.println(llarg);
		llarg = new EnterLlarg("-000000");
		System.out.println(llarg);
		llarg = new EnterLlarg(123456789L);
		System.out.println(llarg);
		llarg = new EnterLlarg(-123456789L);
		System.out.println(llarg);
		byte[] xifres = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6};
		llarg = new EnterLlarg(xifres);
		System.out.println(llarg);
		byte[] xifres2 = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6};
		llarg = new EnterLlarg(xifres2);
		System.out.println(llarg);
	}
	
	public static void main(String[] args) {
		//tests();
		try (Scanner scanner = new Scanner(System.in);){
			System.out.println("Escriu el primer nombre enter a sumar: ");
			String strnum1 = scanner.nextLine();
			EnterLlarg num1 = new EnterLlarg(strnum1);
			System.out.println("Escriu el segon nombre enter a sumar: ");
			String strnum2 = scanner.nextLine();
			EnterLlarg num2 = new EnterLlarg(strnum2);
			System.out.println("Primer enter: "+num1);
			System.out.println("Segon enter: "+num2);
			EnterLlarg suma = num1.suma(num2);
			System.out.println("Suma: "+suma);
		} catch (NumberFormatException e) {
			System.err.println(e.getMessage());
		} catch (UnsupportedOperationException e) {
			System.err.println(e.getMessage());
		}
	}
}
