package cerca_binaria.b;

public class Comprova {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LlistaOrdenada l = new LlistaOrdenada();
		Integer[] nums = {9,0,8,1,7,2,6,3,5,4,1,5,7,3,9};
		for (int i=0; i<nums.length; i++) {
			l.add(nums[i]);
		}
		mostraLlista(l);
		System.out.println();
		
		for (int i=0; i<nums.length; i++) {
			System.out.println("El "+nums[i]+" ocupa la posició "+l.binarySearch(nums[i]));
		}
		
		System.out.println("Posició del 10: "+l.binarySearch(10));
	}
	
	public static void mostraLlista(LlistaOrdenada l) {
		for (int i=0; i<l.size(); i++) {
			System.out.println(i+": "+l.get(i));
		}
	}

}
