package domino;

/**
 * Aquest enum facilita referir-nos a les diferents
 * parts d'una fitxa, o a les posicions on es pot jugar
 * una fitxa a la taula.
 */
public enum Posicio {
	CAP(-1), ESQUERRA(0), DRETA(1);
	
	/*
	 * El codi de la posició: -1 per cap, 0 per esquerra, 1 per dreta.
	 */
	private int code;
	
	private Posicio(int code) {
		this.code = code;
	}
	/**
	 * @return el codi de la posició: -1 per cap, 0 per esquerra, 1 per dreta.
	 */
	public int getCode() {
	    return code;
	}
}
